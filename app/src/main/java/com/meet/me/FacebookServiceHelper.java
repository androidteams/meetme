package com.meet.me;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.meet.me.command.Command;
import com.meet.me.command.facebook.Edge;
import com.meet.me.command.facebook.FacebookGraphCommand;
import com.meet.me.command.facebook.Field;
import com.meet.me.command.facebook.GetAccessTokenCmd;
import com.meet.me.command.facebook.Node;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;


public class FacebookServiceHelper extends ServiceHelper {
    ImageHelper imageHelper;


    public FacebookServiceHelper(Application app) {
        super(app);
        imageHelper = new ImageHelper(); //picture processor wrapper (implement is in progress)
    }

    public boolean checkFacebook(Intent intent, String node) {
        FacebookGraphCommand commandExtra = intent.getParcelableExtra(CommandService.EXTRA_COMMAND);
        return commandExtra != null && commandExtra.getNode().equals(node);
    }

    public boolean checkFacebook(Intent intent, String node, Edge edge) {
        FacebookGraphCommand commandExtra = intent.getParcelableExtra(CommandService.EXTRA_COMMAND);
        return commandExtra != null && commandExtra.getNode().equals(node) && commandExtra.getEdge() == edge;
    }

    public void performAuthorization(String code) {
        executeCommand(new GetAccessTokenCmd(getContext(), code));
    }

    public void getUserInfo(String accessToken, String userId) {
        Bundle params = getUserInfo();
        executeCommand(new FacebookGraphCommand(accessToken, getContext(), userId, params));
    }

    public void getMyInbox(String accessToken) {
        executeCommand(new FacebookGraphCommand(accessToken, getContext(), Node.ME, Edge.INBOX));
    }

    public void getMyProfile(String accessToken) {
        executeCommand(new FacebookGraphCommand(accessToken, getContext(), Node.ME));
    }

    public void getMyFeeds(String accessToken) {
        executeCommand(new FacebookGraphCommand(accessToken, getContext(), Node.ME, Edge.HOME));
    }

    public void getUserFeeds(String accessToken, String userId) {
        executeCommand(new FacebookGraphCommand(accessToken, getContext(), userId, Edge.HOME));
    }

    public void getFriends(String accessToken) {
        Bundle params = getUserInfo();
        executeCommand(new FacebookGraphCommand(accessToken, getContext(), Node.ME, Edge.FRIENDS, params));
    }

    private Bundle getUserInfo() {
        Bundle params = new Bundle();
        List<String> field = new ArrayList<String>();
        field.add("id");
        field.add("name");
        field.add("first_name");
        field.add("last_name");
        field.add("gender");
        field.add("picture.height(320)");

        params.putString("fields", Field.generateField(field));
        return params;
    }

    private void executeCommand(Command request) {
        int requestId = getGeneratedRequestId();
        Intent intent = createIntent(getContext(), request, requestId);
        runRequest(requestId, intent);
    }
}
