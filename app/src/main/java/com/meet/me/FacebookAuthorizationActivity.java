package com.meet.me;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.meet.me.command.facebook.FacebookAuth;
import com.meet.me.command.facebook.FacebookAuthorization;

public class FacebookAuthorizationActivity extends BaseActivity implements FacebookAuth {
    public static final String CLIENT_ID = "client_id";
    public static final String PARAM_REDIRECT_URI = "redirect_uri";
    public static final String PARAM_DIALOG = "dialog";
    public static final String PARAM_OAUTH = "oauth";
    public static final String CODE = "code";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String EXPIRES = "expires";
    public static final String SCOPE = "scope";

    private WebView mWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook_auth);

        mWebView = (WebView) findViewById(R.id.webView);
        initWebView();
    }


    private void initWebView() {
        mWebView.setWebViewClient(new OAuthWebViewClient());

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        mWebView.loadUrl(getFacebookAuthUrl());
    }

    private String getFacebookAuthUrl() {
        Uri.Builder uriBuilder = new Uri.Builder();

        String scope = TextUtils.join(",", getResources().getStringArray(R.array.facebook_scope));

        uriBuilder.scheme(getString(R.string.facebook_schema)).authority(getString(R.string.facebook_auth_authority))
                .appendPath(PARAM_DIALOG).appendPath(PARAM_OAUTH)
                .appendQueryParameter(CLIENT_ID, getString(R.string.facebook_app_id))
                .appendQueryParameter(SCOPE, scope)
                .appendQueryParameter(PARAM_REDIRECT_URI, getString(R.string.facebook_redirect_url));
        return uriBuilder.build().toString();
    }

    private void goToSlidingMenu() {
        startActivity(new Intent(getApplicationContext(), SlidingMenu.class));
        finish();
    }

    @Override
    public void onSuccess() {
        goToSlidingMenu();
    }

    private class OAuthWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(getString(R.string.facebook_redirect_url))) {
                String code = Uri.parse(url).getQueryParameter(CODE);
                if (code != null) {
                    new FacebookAuthorization((MeetMeApplication) getApplicationContext(), code, FacebookAuthorizationActivity.this);
                }
            }
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

    }
}
