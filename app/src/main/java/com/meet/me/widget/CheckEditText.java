package com.meet.me.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.meet.me.CheckInputData;

public class CheckEditText extends FontEditText {
    private CheckInputData mCheckData;

    public CheckEditText(Context context) {
        super(context);
    }

    public CheckEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnCheckInputData(CheckInputData inputData) {
        this.mCheckData = inputData;
    }

    public boolean isCheck() {
        if (mCheckData != null && !TextUtils.isEmpty(getText().toString())) {
            return mCheckData.isInputDataRight(getText().toString());
        }
        return false;
    }
}
