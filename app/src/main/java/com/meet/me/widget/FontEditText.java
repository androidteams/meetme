package com.meet.me.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.meet.me.R;
import com.meet.me.util.TypeFaceUtil;
import com.meet.me.widget.FontTextView;

public class FontEditText extends EditText {
    public FontEditText(Context context) {
        this(context, null);
    }

    public FontEditText(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.fontEditTextStyle);
    }

    public FontEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        readAttributes(attrs, defStyle);
    }

    public void readAttributes(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.FontView, defStyle, R.style.FontEditText);
        String fontPath = null;

        try {
            int fontIndex = typedArray.getInteger(R.styleable.FontView_font, 0);
            fontPath = FontTextView.getFontPath(fontIndex);
        } finally {
            if (typedArray != null) {
                typedArray.recycle();
            }
        }
        setFontTypeFace(fontPath, FontTextView.TEXT_STYLE);
    }

    public void setFontTypeFace(String fontPath, int style) {
        Typeface typeface = TypeFaceUtil.getTypeface(getContext(), fontPath);
        setTypeface(typeface, style);
    }
}
