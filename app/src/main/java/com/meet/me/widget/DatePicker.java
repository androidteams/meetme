package com.meet.me.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.meet.me.R;
import com.meet.me.widget.FontTextView;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class DatePicker extends FontTextView {

    private static final String DD_MMMM_YYYY = "dd MMMM yyyy";

    private GregorianCalendar mBirthday;

    public DatePicker(Context context) {
        this(context, null);
    }

    public DatePicker(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.fontTextStyle);
    }

    public DatePicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mBirthday = new GregorianCalendar();
        setText(getBirthdayText(mBirthday.getTimeInMillis()));
    }

    public void setBirthday(GregorianCalendar birthday) {
        mBirthday = birthday;
        setText(getBirthdayText(mBirthday.getTimeInMillis()));
    }

    public static String getBirthdayText(long millis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DD_MMMM_YYYY, Locale.getDefault());
        return dateFormat.format(millis);
    }
}
