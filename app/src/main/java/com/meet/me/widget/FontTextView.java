package com.meet.me.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import org.holoeverywhere.widget.TextView;

import com.meet.me.R;
import com.meet.me.util.Fonts;
import com.meet.me.util.TypeFaceUtil;

public class FontTextView extends TextView {

    public static final String FORMAT_TTF = "ttf";
    public static final String POINT = ".";
    public static final String FONT_PACKAGE = "fonts";
    public static final int TEXT_STYLE = Typeface.NORMAL;

    public FontTextView(Context context) {
        this(context, null);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.fontTextStyle);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        readAttributes(attrs, defStyle);
    }

    public void readAttributes(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.FontView, defStyle, R.style.FontTextView);
        String fontPath = null;

        try {
            int fontIndex = typedArray.getInteger(R.styleable.FontView_font, 0);
            fontPath = getFontPath(fontIndex);
        } finally {
            if (typedArray != null) {
                typedArray.recycle();
            }
        }
        setFontTypeFace(fontPath, TEXT_STYLE);
    }

    public static String getFontPath(int fontIndex) {
        StringBuilder fontPath = new StringBuilder(FONT_PACKAGE).append("/");
        Fonts[] fonts = Fonts.values();
        if (fontIndex >= 0 && fontIndex < fonts.length) {
            fontPath.append(fonts[fontIndex].getFontName());
        } else {
            fontPath.append(fonts[0].getFontName());
        }

        return fontPath.append(POINT).append(FORMAT_TTF).toString();
    }

    public void setFontTypeFace(String fontPath, int style) {
        Typeface typeface = TypeFaceUtil.getTypeface(getContext(), fontPath);
        setTypeface(typeface, style);
    }
}
