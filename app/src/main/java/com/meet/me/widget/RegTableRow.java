package com.meet.me.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableRow;

import com.meet.me.R;
import com.meet.me.widget.FontTextView;

public class RegTableRow extends TableRow {

    public RegTableRow(Context context) {
        this(context, null);
    }

    public RegTableRow(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttributes(attrs);
    }

    public void readAttributes(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RegView, 0, 0);
        try {
            int layout = typedArray.getInteger(R.styleable.RegView_titleLayout, R.layout.reg_title);
            String title = typedArray.getString(R.styleable.RegView_regTitle);
            View rootView = mergeView(layout);

            if (!TextUtils.isEmpty(title)) {
                ((FontTextView) rootView.findViewById(R.id.title)).setText(title);
            }
        } finally {
            if (typedArray != null) {
                typedArray.recycle();
            }
        }
    }

    public View mergeView(int layout) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layout, this, true);
    }
}
