package com.meet.me.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meet.me.R;
import com.meet.me.widget.FontTextView;

public class MenuAdapter extends SectionAdapter {

    public MenuAdapter(Context context, Section... sections) {
        super(context);
        for (Section section : sections) {
            addSection(section);
        }
    }

    @Override
    public View getHeaderView(String headerName, int index, int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.header, parent, false);

        FontTextView header = (FontTextView) convertView.findViewById(R.id.header);
        header.setText(headerName);
        return convertView;
    }
}
