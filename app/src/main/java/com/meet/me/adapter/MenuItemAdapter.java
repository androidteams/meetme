package com.meet.me.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meet.me.R;

import java.util.List;

public class MenuItemAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<MenuItem> mMenuItems;

    public MenuItemAdapter(Context context, List<MenuItem> menuItems) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuItems = menuItems;
    }

    @Override
    public int getCount() {
        return mMenuItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mMenuItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MenuHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.menu_item, null);

            holder = new MenuHolder();
            holder.mImage = (ImageView) convertView.findViewById(R.id.icon);
            holder.mTitle = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        } else {
            holder = (MenuHolder) convertView.getTag();
        }

        MenuItem menuItem = mMenuItems.get(position);

        if (holder != null) {
            holder.mTitle.setText(menuItem.getTitle());
            holder.mImage.setImageResource(menuItem.getIcon());
        }

        return convertView;
    }

    static class MenuHolder {
        public ImageView mImage;
        public TextView mTitle;
    }

    public static class MenuItem {
        private String mTitle;
        private int mIcon;
        private Fragment mFragment;

        public MenuItem(String title, int icon, Fragment fragment) {
            this.mTitle = title;
            this.mIcon = icon;
            this.mFragment = fragment;
        }

        public String getTitle() {
            return mTitle;
        }

        public int getIcon() {
            return mIcon;
        }

        public Fragment getFragment() {
            return mFragment;
        }
    }
}
