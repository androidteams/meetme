package com.meet.me.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.meet.me.R;
import com.meet.me.database.tables.User;

import java.util.List;

public class FriendsAdapter extends BaseAdapter {

    private List<User> mFriends;
    private LayoutInflater mInflater;

    public FriendsAdapter(Context context, List<User> friends) {
        this.mFriends = friends;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setFriends(List<User> frds) {
        mFriends = frds;
    }


    @Override
    public int getCount() {
        return mFriends.size();
    }

    @Override
    public Object getItem(int position) {
        return mFriends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        public ImageView mImage;
        public TextView mTitle;
        public Button mButton;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView  == null) {

            convertView = mInflater.inflate(R.layout.friend_item, null);

            viewHolder = new ViewHolder();
            viewHolder.mImage = (ImageView) convertView.findViewById(R.id.img);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String titleName = mFriends.get(position).getName();

        if (viewHolder != null) {
            viewHolder.mTitle.setText(titleName);
        }
        return convertView;
    }
}
