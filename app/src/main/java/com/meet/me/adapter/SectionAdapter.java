package com.meet.me.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public abstract class SectionAdapter extends BaseAdapter {
    private final static int TYPE_SECTION_HEADER = 0;

    private Context mContext;
    private List<Section> mSections;

    protected SectionAdapter(Context context) {
        super();
        mContext = context;
        mSections = new ArrayList<Section>();
    }

    public void addSection(Section section) {
        mSections.add(section);
    }

    public void removeSection(int index) {
        mSections.remove(index);
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public int getCount() {
        int total = 0;
        for (Section section : mSections) {
            total += section.getAdapter().getCount() + (section.hasHeader() ? 1 : 0);
        }
        return  total;
    }

    @Override
    public Object getItem(int position) {
        for (Section section : mSections) {
            Adapter adapter = section.getAdapter();
            int count = adapter.getCount();
            if (count == 0) continue;
            if (position == 0 && section.hasHeader()) {
                return section;
            }

            int size = section.hasHeader() ? count + 1 : count;
            if (position < size) {
                if (section.hasHeader()) {
                    position--;
                }
                return section.getAdapter().getItem(position);
            }
            position -= size;
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int sectionIndex = 0;

        for (Section section : mSections) {
            Adapter adapter = section.getAdapter();
            int count = adapter.getCount();
            if (count == 0) {
                sectionIndex++;
                continue;
            }

            if (position == 0 && section.hasHeader()) {
                return getHeaderView(section.getTitle(), sectionIndex, position, convertView, parent);
            }

            int size = section.hasHeader() ? count + 1 : count;
            if (position < size) {
                if (section.hasHeader()) {
                    position--;
                }
                return adapter.getView(position, convertView, parent);
            }
            position -= size;
            sectionIndex++;
        }
        return null;
    }

    @Override
    public int getViewTypeCount() {
        int total = 1;
        for (Section section : mSections) {
            total += section.getAdapter().getViewTypeCount();
        }
        return total;
    }

    @Override
    public int getItemViewType(int position) {
        int offset = 1;
        for (Section section : mSections) {
            Adapter adapter = section.getAdapter();
            int count = adapter.getCount();
            if (count == 0) {
                offset += adapter.getViewTypeCount();
                continue;
            }

            if (position == 0 && section.hasHeader()) {
                return TYPE_SECTION_HEADER;
            }

            int size = section.hasHeader() ? count + 1 : count;
            if (position < size) {
                if (section.hasHeader()) {
                    position--;
                }
                return offset + adapter.getItemViewType(position);
            }
            position -= size;
            offset += adapter.getViewTypeCount();
        }
        return -1;
    }

    public abstract View getHeaderView(String headerName, int index, int position, View convertView, ViewGroup parent);

    public static class Section {
        private final Adapter mAdapter;
        private final String mTitle;
        private final boolean mHasHeader;

        public Section(String title, Adapter adapter, boolean needHeader) {
            this.mHasHeader = needHeader;
            this.mTitle = title;
            this.mAdapter = adapter;
        }

        public Section(String title, Adapter adapter) {
            this(title, adapter, true);
        }

        public Section(Adapter adapter) {
            this("", adapter, false);
        }

        public String getTitle() {
            return mTitle;
        }

        public boolean hasHeader() {
            return mHasHeader;
        }

        public Adapter getAdapter() {
            return mAdapter;
        }
    }
}
