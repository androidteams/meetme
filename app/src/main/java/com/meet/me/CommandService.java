package com.meet.me;

import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.util.SparseArray;

import com.meet.me.command.Command;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CommandService extends Service {
    private static final int NUM_THREADS = 4;

    public static final String ACTION_EXECUTE_COMMAND = MeetMeApplication.PACKAGE.concat(".ACTION_EXECUTE_COMMAND");

    public static final String EXTRA_COMMAND = MeetMeApplication.PACKAGE.concat(".EXTRA_COMMAND");
    public static final String EXTRA_REQUEST_ID = MeetMeApplication.PACKAGE.concat(".EXTRA_REQUEST_ID");
    public static final String EXTRA_STATUS_RECEIVER = MeetMeApplication.PACKAGE.concat(".STATUS_RECEIVER");

    private ExecutorService mExecutor;

    private SparseArray<RunningCommand> mRunningCommands;

    @Override
    public void onCreate() {
        super.onCreate();
        mExecutor = Executors.newFixedThreadPool(NUM_THREADS);
        mRunningCommands = new SparseArray<RunningCommand>();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {//intent != null
        if (intent != null) {
            if (intent.getAction().equals(ACTION_EXECUTE_COMMAND)) {
                RunningCommand runningCommand = new RunningCommand(intent);

                mRunningCommands.append(getCommandId(intent), runningCommand);
                mExecutor.submit(runningCommand);
            }
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mExecutor.shutdown();
    }

    private Command getCommand(Intent intent) {
        return intent.getParcelableExtra(EXTRA_COMMAND);
    }

    private int getCommandId(Intent intent) {
        return intent.getIntExtra(EXTRA_REQUEST_ID, -1);
    }

    private ResultReceiver getReceiver(Intent intent) {
        return intent.getParcelableExtra(EXTRA_STATUS_RECEIVER);
    }

    private class RunningCommand implements Runnable {

        private Intent mIntent;
        private Command command;

        private RunningCommand(Intent intent) {
            mIntent = intent;
            command = getCommand(intent);
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            command.execute(getApplicationContext(), getReceiver(mIntent));
        }

        private void shutdown() {
            synchronized (mRunningCommands) {
                mRunningCommands.remove(getCommandId(mIntent));
                if (mRunningCommands.size() == 0) {
                    stopSelf();
                }
            }
        }

    }
}