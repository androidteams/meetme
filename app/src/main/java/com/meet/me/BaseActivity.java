package com.meet.me;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.meet.me.command.api.*;

import org.holoeverywhere.app.Activity;

public class BaseActivity extends Activity implements RequestListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((MeetMeApplication) getApplicationContext()).getServiceHelper().addListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ((MeetMeApplication) getApplicationContext()).getServiceHelper().removeListener(this);
    }

    @Override
    public void onCompleted(int requestId, Intent requestIntent, Response response) {
        Log.d("TAG", "onComplete");
    }

    @Override
    public void onError(int requestId, Intent requestIntent, com.meet.me.command.api.Error error) {
        Log.d("TAG", "onError");
    }

    @Override
    public void onProgress(int requestId, Intent requestIntent, HttpRequest.ProgressType progressType, long bytesLoaded, long bytesTotal) {
        Log.d("TAG", "omProgress");
    }
}
