package com.meet.me.command;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.meet.me.MeetMeApplication;

public abstract class Command implements Parcelable {
    public static String EXTRA_PROGRESS = MeetMeApplication.PACKAGE.concat(".EXTRA_PROGRESS");

    public static final int RESPONSE_SUCCESS = 0;
    public static final int RESPONSE_FAILURE = 1;
    public static final int RESPONSE_PROGRESS = 2;
    private boolean mCanceled;

    private Context mContext;

    private int mExecutionNumber;

    private ResultReceiver mCallback;

    public Command() {
        super();
        this.mCanceled = false;
    }



    /**
     * Called after execution has been requested. Should make
     * the command code to be run. For long running operations
     * you should always see {@link #isCanceled()} and stop the
     * command as soon as possible.
     */
    protected abstract void onExecute();

    /**
     * Called during the command execution instantly after
     * {@link #onExecute()} method. Subclasses should override
     * this method in order to post results and notify targets
     * about command completion.
     * */
    protected void onDone() {
        //empty
    }

    protected void notifySuccess(Bundle data) {
        sendUpdate(RESPONSE_SUCCESS, data);
    }

    protected void notifyFailure(Bundle data) {
        sendUpdate(RESPONSE_FAILURE, data);
    }

    protected void sendProgress(int progress) {
        Bundle data = new Bundle();
        data.putInt(EXTRA_PROGRESS, progress);

        sendUpdate(RESPONSE_PROGRESS, data);
    }

    public void sendUpdate(int resultCode, Bundle data) {
        if (mCallback != null) {
            mCallback.send(resultCode, data);
        }
    }

    /**
     * Starts command execution
     * @see #onExecute()
     * @see #onDone()
     * */
    public final void execute(Context context, ResultReceiver callback) {
        mCallback = callback;
        mContext = context;
        if (!isCanceled()) {
            incrementExecutionNumber();
            onExecute();
            onDone();
        }
    }

    public final void execute(ResultReceiver callback) {
        mCallback = callback;
        if (!isCanceled()) {
            incrementExecutionNumber();
            onExecute();
            onDone();
        }
    }

    public Context getContext() {
        return mContext;
    }

    int getExecutionCount(){
        synchronized (this){
            return mExecutionNumber;
        }
    }

    public boolean isCanceled() {
        synchronized (this) {
            return mCanceled;
        }
    }

    public void incrementExecutionNumber() {
        synchronized (this) {
            mExecutionNumber++;
        }
    }

    /**
     * Requests cancellation of the command.
     * <br>
     * <b>Note:</b> There is no guarantee that the command
     * will be cancelled immediately or will be cancelled
     * at all. This method just requests the cancellation.
     * While implementing you should see {@link #isCanceled()}
     * in order to handle cancellation of the command.
     * @see #isCanceled()
     */
    public void cancel() {
        synchronized (this) {
            mCanceled = true;
        }
    }
}
