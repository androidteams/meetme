package com.meet.me.command.facebook;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;


import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.meet.me.FacebookAuthorizationActivity;
import com.meet.me.FacebookServiceHelper;
import com.meet.me.MeetMeApplication;
import com.meet.me.command.api.Error;
import com.meet.me.command.api.HttpRequest;
import com.meet.me.command.api.RequestListener;
import com.meet.me.command.api.Response;
import com.meet.me.database.DatabaseManager;
import com.meet.me.database.tables.Session;
import com.meet.me.database.tables.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.Date;

public class FacebookAuthorization {


    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String GENDER = "gender";
    public static final String FEMALE = "female";
    private Context mContext;

    private String mAccessToken;
    private String mExpires;

    private FacebookServiceHelper serviceHelper;


    private FacebookAuth mFacebookAuth;


    public boolean isSession() {
        return !TextUtils.isEmpty(mAccessToken) && !TextUtils.isEmpty(mExpires);
    }

    RequestListener mGetAccessToken = new RequestListener() {
        @Override
        public void onCompleted(int requestId, Intent requestIntent, Response response) {

            if (serviceHelper.check(requestIntent, GetAccessTokenCmd.class)) {
                mAccessToken = response.getValue(FacebookAuthorizationActivity.ACCESS_TOKEN);
                mExpires = response.getValue(FacebookAuthorizationActivity.EXPIRES);

                if (isSession()) {
                    serviceHelper.getMyProfile(mAccessToken);

                }
            } else if (serviceHelper.checkFacebook(requestIntent, Node.ME)) {
                try {
                    JSONObject data = new JSONObject(response.getResponseData());
                    Integer clientId = data.getInt(ID);
                    String name = data.getString(NAME);
                    String firstName = data.getString(FIRST_NAME);
                    String lastName = data.getString(LAST_NAME);
                    Boolean isMale = true;

                    if (data.getString(GENDER).equals(FEMALE)) {
                        isMale = false;
                    }

                    User user = new User(clientId, name, firstName, lastName,null,isMale);
                    RuntimeExceptionDao<User, Integer> usersDao = DatabaseManager.getInstance().getHelper().getUserDao();

                    if (!usersDao.idExists(clientId)) {
                        usersDao.create(user);
                    } else {
                        usersDao.update(user);
                    }

                    Session session = new Session(clientId, mAccessToken, new Date(Long.parseLong(mExpires) + System.currentTimeMillis()), user);

                    RuntimeExceptionDao<Session, Integer> sessionDao = DatabaseManager.getInstance().getHelper().getSessionDao();
                    if (!sessionDao.idExists(clientId)) {
                        sessionDao.create(session);
                    } else {
                        sessionDao.update(session);
                    }

                    mFacebookAuth.onSuccess();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        /*
        {
"error":{
"message":"(#298) Requires extended permission: read_mailbox",
"type":"OAuthException",
"code":298
}
}
        * */
        @Override
        public void onError(int requestId, Intent requestIntent, Error error) {
            try {
                JSONObject jsError = new JSONObject(error.getErrorMessage());
                JSONObject err = jsError.getJSONObject("error");
                String message = err.getString("message");
                String errType = err.getString("type");
                Integer code = err.getInt("code");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onProgress(int requestId, Intent requestIntent, HttpRequest.ProgressType progressType, long bytesLoaded, long bytesTotal) {

        }
    };

    public FacebookAuthorization(Context context, String code, FacebookAuth facebookAuth) {
        mContext = context;

        mFacebookAuth = facebookAuth;
        serviceHelper = MeetMeApplication.getApplication(mContext).getServiceHelper();
        serviceHelper.addListener(mGetAccessToken);
        serviceHelper.performAuthorization(code);
    }
}
