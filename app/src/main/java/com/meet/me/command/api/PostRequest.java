package com.meet.me.command.api;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.meet.me.command.facebook.GetAccessTokenCmd;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PostRequest extends ParameterRequest {

    public static final String UTF_8 = "UTF-8";

    public static final Parcelable.Creator<ParameterRequest> CREATOR = new Parcelable.Creator<ParameterRequest>() {

        @Override
        public ParameterRequest createFromParcel(Parcel source) {
            return new PostRequest(source);
        }

        @Override
        public ParameterRequest[] newArray(int size) {
            return new ParameterRequest[0];
        }
    };

    public PostRequest(Bundle parameters) {
        super(parameters);
    }

    public PostRequest(Parcel parcel) {
        super(parcel);
    }

    @Override
    public String buildUrl(Uri.Builder uri) {
        return uri.build().toString();
    }

    @Override
    void onPrepareConnection(HttpURLConnection connection) {
        try {
            connection.setRequestMethod(HttpMethod.POST.name());
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            if (getParameters() != null) {
                UrlEncodedFormEntity body = new UrlEncodedFormEntity(getNameValueParameters(), UTF_8);
                encodeRequestBody(connection, body);
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public List<NameValuePair> getNameValueParameters() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        if (params != null) {
            Iterator<String> keyIterator = getParameters().keySet().iterator();
            String key;

            while (keyIterator.hasNext()) {
                key = keyIterator.next();
                params.add(new BasicNameValuePair(key, getParameters().get(key).toString()));
            }
        }
        return params;
    }

    private void encodeRequestBody(HttpURLConnection connection, UrlEncodedFormEntity body) throws UnsupportedEncodingException {
        if (body != null) {
            OutputStream outStream = null;
            try {
                outStream = connection.getOutputStream();
                body.writeTo(outStream);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (outStream != null){
                    try {
                        outStream.flush();
                        outStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
