package com.meet.me.command.facebook;

public class Facebook {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String GENDER = "gender";
    public static final String FEMALE = "female";
    public static final String PICTURE = "picture";
    public static final String FIELDS = "fields";
    public static final String DATA = "data";
    public static final String URL = "url";
}
