package com.meet.me.command.api;

import android.net.Uri;
import android.os.Bundle;

import com.meet.me.command.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/**
 * Single HTTP request
 * */
public abstract class HttpRequest extends Command {

    public static final String USER_AGENT = "User-Agent";
    public static final String ACCEPT_ENCODING = "Accept-Encoding";
    public static final String GZIP = "gzip";
    public static final String EXTRA_RESPONSE = "RESPONSE";
    public static final String EXTRA_BITMAP = "BITMAP";

    public enum ProgressType {
        Download,
        Upload
    }

    private HttpURLConnection mConnection;

    @Override
    protected void onExecute() {
        String buildUrl = buildUrl(createBuilder());
        try {
            URL url = new URL(buildUrl);
            mConnection = (HttpURLConnection) url.openConnection();
            setDefaultHeader();
            onPrepareConnection(mConnection);
            mConnection.connect();
            onPostExecuteRequest(mConnection);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Use to create common interface for requests. Schema and authority.
     * */
    public Uri.Builder createBuilder() {
        return  new Uri.Builder();
    }

    /**
     * Use to build specific URI. Path, Http parameters.
     * */
    public abstract String buildUrl(Uri.Builder uri);

    private void setDefaultHeader() {
        mConnection.setRequestProperty(USER_AGENT, "meetme");
        mConnection.setRequestProperty(ACCEPT_ENCODING, GZIP);
    }

    public void onPostExecuteRequest(HttpURLConnection mConnection) {
        try {
            Bundle responseData = new Bundle();
            HttpCode httpCode = HttpCode.getHttpCode(mConnection.getResponseCode());

            if (httpCode.equals(HttpCode.OK)) {
                responseData.putString(EXTRA_RESPONSE, getResponseData(getInputStream()));
                notifySuccess(responseData);
            } else {
                responseData.putString(EXTRA_RESPONSE, getResponseData(mConnection.getErrorStream()));
                notifyFailure(responseData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected InputStream getInputStream() throws IOException {
        String contentEncoding = mConnection.getContentEncoding();

        if (contentEncoding == null || !contentEncoding.equalsIgnoreCase(GZIP)) {
            return mConnection.getInputStream();
        }
        return new GZIPInputStream(mConnection.getInputStream());
    }

    protected String getResponseData(InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }


    public void repeatRequest() {
        //TODO: perform when repeatRequest will be use(for example: after Capcha)
    }

    abstract void onPrepareConnection(HttpURLConnection connection) throws ProtocolException, UnsupportedEncodingException;
}
