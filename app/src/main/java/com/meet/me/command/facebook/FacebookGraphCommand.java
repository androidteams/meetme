package com.meet.me.command.facebook;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.meet.me.R;
import com.meet.me.command.api.GetRequest;
import com.meet.me.command.api.ParameterRequest;

public class FacebookGraphCommand extends GetRequest {

    private Context mContext;

    private String mNode;
    private Edge mEdge;
    private Bundle mHttpParams;


    public static final Parcelable.Creator<ParameterRequest> CREATOR = new Parcelable.Creator<ParameterRequest>() {

        @Override
        public ParameterRequest createFromParcel(Parcel source) {
            return new FacebookGraphCommand(source);
        }

        @Override
        public ParameterRequest[] newArray(int size) {
            return new ParameterRequest[0];
        }
    };

    public FacebookGraphCommand(Parcel parcel) {
        super(parcel);
        mNode = parcel.readString();
        mEdge = (Edge) parcel.readSerializable();
    }

    public FacebookGraphCommand(String accessToken, Context context, String node) {
        this(accessToken, context, node, new Bundle());
        addHttpParameters("access_token", accessToken);
    }

    public FacebookGraphCommand(String accessToken, Context context, String node, Bundle params) {
        this(context, node, Edge.UNDEFINED, params);
        addHttpParameters("access_token", accessToken);
    }

    public FacebookGraphCommand(String accessToken, Context context, String node, Edge edge, Bundle params) {
        this(context, node, edge, params);
        addHttpParameters("access_token", accessToken);
    }

    public FacebookGraphCommand(String accessToken, Context context, String node, Edge edge) {
        this(context, node, edge, null);
        addHttpParameters("access_token", accessToken);
    }

    public FacebookGraphCommand(Context context, String node, Edge edge, Bundle params) {
        super(params);
        mContext = context;
        mNode = node;
        mEdge = edge;
        mHttpParams = params;
    }

    @Override
    public Uri.Builder createBuilder() {
        return new Uri.Builder().scheme(getContext().getString(R.string.facebook_schema))
                .authority(getContext().getString(R.string.facebook_authority));
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mNode);
        dest.writeSerializable(mEdge);
    }

    @Override
    public String buildUrl(Uri.Builder uri) {
        if (mNode == null) {
            return null;
        }

        uri.appendPath(mNode);

        if (mEdge != Edge.UNDEFINED) {
            uri.appendPath(mEdge.getEdge());
        }

        return super.buildUrl(uri);
    }

    public String getNode() {
        return mNode;
    }

    public Edge getEdge() {
        return mEdge;
    }
}
