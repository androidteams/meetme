package com.meet.me.command.api;

import android.os.Bundle;

public class Response {
    private HttpRequest mHttpRequest;
    private HttpCode mHttpCode;
    private String mResponseData;
    private Bundle bundle;

    public Response(HttpCode mHttpCode) {
        this.mHttpCode = mHttpCode;
    }

    public Response(HttpCode mHttpCode, String mResponseData, Bundle bundle) {
        this.mHttpCode = mHttpCode;
        this.mResponseData = mResponseData;
        this.bundle = bundle;
    }

    public String getResponseData() {
        return mResponseData;
    }

    public Bundle getBundle()
    {
        return bundle;
    }

    public String getValue(String key) {
        String[] params = mResponseData.split("&");
        String[] pair;
        for (String param : params) {
            pair = param.split("=");
            if (pair[0].equals(key)) {
                return  pair[1];
            }
        }
        return null;
    }
}
