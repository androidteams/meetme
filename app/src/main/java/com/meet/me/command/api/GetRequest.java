package com.meet.me.command.api;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;

import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.util.Iterator;
import java.util.Set;

public class GetRequest extends ParameterRequest {

    public static final Parcelable.Creator<ParameterRequest> CREATOR = new Parcelable.Creator<ParameterRequest>() {

        @Override
        public ParameterRequest createFromParcel(Parcel source) {
            return new GetRequest(source);
        }

        @Override
        public ParameterRequest[] newArray(int size) {
            return new ParameterRequest[0];
        }
    };

    public GetRequest() {
        super(new Bundle());
    }

    public GetRequest(Parcel parcel) {
        super(parcel);
    }

    public GetRequest(Bundle parameters) {
        super(parameters);
    }

    public void addHttpParameters(String key, String value) {
        Bundle param = getParameters();
        param.putString(key, value);
    }

    @Override
    public String buildUrl(Uri.Builder uri) {
        if (getParameters() != null) {
            Iterator<String> keyIterator = getParameters().keySet().iterator();
            String key;

            while (keyIterator.hasNext()) {
                key = keyIterator.next();
                uri.appendQueryParameter(key, getParameters().get(key).toString());
            }
        }
        return uri.build().toString();
    }

    @Override
    void onPrepareConnection(HttpURLConnection connection) {
        try {
            connection.setRequestMethod(HttpMethod.GET.name());
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
    }
}
