package com.meet.me.command.facebook;

import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.meet.me.R;
import com.meet.me.command.api.GetRequest;
import com.meet.me.command.api.ParameterRequest;
import com.meet.me.command.api.RequestListener;
import com.meet.me.FacebookAuthorizationActivity;

public class GetAccessTokenCmd extends GetRequest {
    public static final String CLIENT_SECRET = "client_secret";
    public static final String PARAM_ACCESS_TOKEN = "access_token";

    private Context mContext;

    public static final Parcelable.Creator<ParameterRequest> CREATOR = new Parcelable.Creator<ParameterRequest>() {

        @Override
        public ParameterRequest createFromParcel(Parcel source) {
            return new GetAccessTokenCmd(source);
        }

        @Override
        public ParameterRequest[] newArray(int size) {
            return new ParameterRequest[0];
        }
    };

    public GetAccessTokenCmd(Parcel parcel) {
        super(parcel);
    }

    public GetAccessTokenCmd(Context context, String code) {
        mContext = context;
        setDefaultFacebookHttpParameters(code);
    }

    @Override
    public Uri.Builder createBuilder() {
        Uri.Builder urBuilder = new Uri.Builder();
        return urBuilder.scheme(getContext().getString(R.string.facebook_schema))
                .authority(getContext().getString(R.string.facebook_authority))
                .appendPath(FacebookAuthorizationActivity.PARAM_OAUTH)
                .appendPath(PARAM_ACCESS_TOKEN);
    }

    private void setDefaultFacebookHttpParameters(String code) {
        addHttpParameters(FacebookAuthorizationActivity.CODE, code);
        addHttpParameters(FacebookAuthorizationActivity.CLIENT_ID, mContext.getString(R.string.facebook_app_id));
        addHttpParameters(FacebookAuthorizationActivity.PARAM_REDIRECT_URI, mContext.getString(R.string.facebook_redirect_url));
        addHttpParameters(CLIENT_SECRET, mContext.getString(R.string.facebook_app_secret));
    }
}
