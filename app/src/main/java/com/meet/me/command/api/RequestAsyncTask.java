package com.meet.me.command.api;

import android.os.AsyncTask;

import com.meet.me.command.Command;

public class RequestAsyncTask extends AsyncTask<Command, Void, Void> {

    private Command mCommand;

    public RequestAsyncTask(Command command) {
        super();
        mCommand = command;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Command... params) {

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}
