package com.meet.me.command;

import android.os.Parcel;
import android.os.ResultReceiver;

import java.util.LinkedList;

public abstract class CommandGroup extends Command {
    /**
     * Defines how much time each instance of the {@link Command}
     * can be executed.
     */
    private static final int INFINITE_LOOP_WARN = 5;

    /**
     * Chain of the commands that will be executed
     */
    private final LinkedList<ResultCommand> mCommandChain;

    private ResultCommand mCurrent;

    public CommandGroup() {
        mCommandChain = new LinkedList<ResultCommand>();
    }

    @Override
    protected void onExecute() {
        ResultCommand cmd;
        while ((cmd = getNextCommand()) != null) {
            if (cmd.command.getExecutionCount() >= INFINITE_LOOP_WARN) {
                break;
            }
            onExecuteCommand(cmd);
        }
    }

    private ResultCommand getNextCommand() {
        synchronized (this) {
            if (!isCanceled() && hasMoreCommands()) {
                mCurrent = mCommandChain.peek();
                return mCurrent;
            }
            return null;
        }
    }

    private boolean hasMoreCommands(){
        synchronized (this) {
            return !mCommandChain.isEmpty();
        }
    }

    protected void onExecuteCommand(ResultCommand resultCommand) {
        resultCommand.command.execute(resultCommand.resultReceiver);
        synchronized (this) {
            mCommandChain.remove(resultCommand);
        }
    }

    public void removeAllCommand() {
        synchronized (this) {
            mCommandChain.clear();
        }
    }

    protected void addCommand(Command cmd, ResultReceiver resultReceiver) {
        synchronized (this) {
            mCommandChain.addLast(new ResultCommand(cmd, resultReceiver));
        }
    }

    protected void addCommandAtFront(Command cmd, ResultReceiver resultReceiver) {
        synchronized (this) {
            mCommandChain.addFirst(new ResultCommand(cmd, resultReceiver));
        }
    }

    @Override
    public void cancel() {
        synchronized (this) {
            super.cancel();
            if(mCurrent != null) {
                mCurrent.command.cancel();
            }
        }
    }

    private class ResultCommand {
        Command command;
        ResultReceiver resultReceiver;

        private ResultCommand(Command command, ResultReceiver resultReceiver) {
            this.command = command;
            this.resultReceiver = resultReceiver;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
