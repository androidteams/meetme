package com.meet.me.command.facebook;

public enum Edge {
    FRIENDS("friends"), HOME("home"), INBOX("inbox"), UNDEFINED("undefined");

    private String mEdge;
    Edge(String node) {
        mEdge = node;
    }

    public String getEdge() {
        return mEdge;
    }
}
