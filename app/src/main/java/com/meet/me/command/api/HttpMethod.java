package com.meet.me.command.api;
/**
 * Enumeration of HTTP methods supported by RequestData
 */
public enum HttpMethod {
    /**
     * Use Http method "GET" for the request
     */
    GET,
    /**
     * Use Http method "POST" for the request
     */
    POST
}
