package com.meet.me.command.api;

import org.json.JSONObject;

import java.util.Map;

public class Error {
    public static final int API_ERROR = -101;
    public static final int API_CANCELED = -102;
    public static final int API_REQUEST_NOT_PREPARED = -103;
    public static final int API_JSON_FAILED = -104;
    public static final int API_REQUEST_HTTP_FAILED = -105;

    public HttpCode mHttpCode;
    public String mErrorMessage;
    public HttpRequest mHttpRequest;
    /**
     * Captcha identifier for captcha-check
     */
    public String mCaptchaSid;
    /**
     * Image for captcha-check
     */
    public String mCaptchaImg;
    /**
     * Redirection address if validation check required
     */
    public String redirectUri;

    /**
     * Generate new error with code
     *
     * @param errorCode if it's an HTTP error. Negative if it's API or SDK error
     */
    public Error(HttpCode errorCode, String responseData) {
        mHttpCode = errorCode;
        mErrorMessage = responseData;
    }

    /**
     * Error from JSON
     * @param jsonError
     */
    public Error(JSONObject jsonError) {

    }

    public Error(Map<String, String> queryParams) {

    }


    public HttpCode getHttpCode() {
        return mHttpCode;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }
}
