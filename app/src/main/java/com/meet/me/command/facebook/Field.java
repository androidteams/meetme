package com.meet.me.command.facebook;

import java.util.List;

public class Field {
    public static String generateField(List<String> field) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < field.size(); i++) {
            result.append(field.get(i));

            if (i != field.size() - 1) {
                result.append(",");
            }
        }
        return result.toString();
    }
}
