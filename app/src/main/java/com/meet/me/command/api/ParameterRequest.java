package com.meet.me.command.api;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.meet.me.command.facebook.GetAccessTokenCmd;

public abstract class ParameterRequest extends HttpRequest {
    private Bundle mParameters;

    public ParameterRequest(Bundle parameters) {
        mParameters = parameters;
    }

    public ParameterRequest(Parcel parcel) {
        mParameters = parcel.readBundle();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeBundle(mParameters);
    }


    public Bundle getParameters() {
        if (mParameters == null) {
            mParameters = new Bundle();
        }
        return mParameters;
    }
}
