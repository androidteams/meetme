package com.meet.me.command.api;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * RequestBatch contains a list of Request objects that can be sent to Facebook in a single round-trip.
 */
public class HttpRequestBatch extends AbstractList<HttpRequest> {

    private List<HttpRequest> requests = new ArrayList<HttpRequest>();

    /**
     * Constructor. Creates an empty batch.
     */
    public HttpRequestBatch() {
        this.requests = new ArrayList<HttpRequest>();
    }

    /**
     * Constructor.
     * @param requests the requests to add to the batch
     */
    public HttpRequestBatch(Collection<HttpRequest> requests) {
        this.requests = new ArrayList<HttpRequest>(requests);
    }

    /**
     * Constructor.
     * @param requests the requests to add to the batch
     */
    public HttpRequestBatch(HttpRequest... requests) {
        this.requests = Arrays.asList(requests);
    }

    @Override
    public final boolean add(HttpRequest request) {
        return requests.add(request);
    }

    @Override
    public final void add(int location, HttpRequest request) {
        requests.add(location, request);
    }

    @Override
    public final void clear() {
        requests.clear();
    }

    @Override
    public final HttpRequest get(int i) {
        return requests.get(i);
    }

    @Override
    public final HttpRequest remove(int location) {
        return requests.remove(location);
    }

    @Override
    public final HttpRequest set(int location, HttpRequest request) {
        return requests.set(location, request);
    }

    @Override
    public final int size() {
        return requests.size();
    }

    public interface RequestBatchListener {
        /**
         * The method that will be called when a batch completes.
         *
         * @param batch     the RequestBatch containing the Requests which were executed
         */
        void onBatchCompleted(HttpRequestBatch batch);
    }
}
