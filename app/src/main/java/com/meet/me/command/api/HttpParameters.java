package com.meet.me.command.api;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HttpParameters {
    private List<NameValuePair> mParams = new ArrayList<NameValuePair>();

    public void add(String key, String[] values) {
        String strValues = converListToString(values);
        mParams.add(new BasicNameValuePair(key, strValues));
    }

    public void add(String key, Object value) {
        mParams.add(new BasicNameValuePair(key, value.toString()));
    }

    public Iterator<NameValuePair> getIterator() {
        return mParams.iterator();
    }

    public List<NameValuePair> getParameters() {
        return mParams;
    }

    private String converListToString(String[] values) {
        StringBuilder result = new StringBuilder();
        int listSize = values.length;

        for (int i = 0; i < listSize; i++) {
            result.append(values[i]);
            if (i == listSize - 1) {
                result.append(",");
            }
        }
        return result.toString();
    }
}
