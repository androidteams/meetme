package com.meet.me.command.api;

import android.content.Intent;

/**
 * Specifies the interface that consumers of the HttpRequest class can implement in order to be notified when a
 * particular request completes, either successfully or with an error.
 * */
public interface RequestListener {

    /**
     * The method that will be called when a request completes without errors.
     *
     * @param response response from HttpRequest
     * */
    public abstract void onCompleted(int requestId, Intent requestIntent, Response response);

    /**
     * Called immediately if there was HTTP error
     *
     * @param error error for HttpRequest
     */
    public void onError(int requestId, Intent requestIntent, Error error);

    /**
     * Specify progress for uploading or downloading. Useless for text requests (because gzip encoding bytesTotal will always return -1)
     *
     * @param progressType type of progress (upload or download)
     * @param bytesLoaded  total bytes loaded
     * @param bytesTotal   total bytes suppose to be loaded
     */
    public void onProgress(int requestId, Intent requestIntent, HttpRequest.ProgressType progressType, long bytesLoaded, long bytesTotal);
}