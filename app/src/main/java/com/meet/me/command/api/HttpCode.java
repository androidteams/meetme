package com.meet.me.command.api;

public enum HttpCode {
    UNDEFINED(-1), OK(200), REDIRECT(300), BAD_REQUEST(400), FORBIDDEN(403), INTERNAL_SERVER_ERROR(500);

    private int mCode;
    HttpCode(int code) {
        this.mCode = code;
    }

    public int getCode() {
        return mCode;
    }

    public static HttpCode getHttpCode(int code) {
        for (HttpCode httpCode : HttpCode.values()) {
            if (code == httpCode.getCode()) {
                return httpCode;
            }
        }
        return UNDEFINED;
    }
}
