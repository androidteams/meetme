package com.meet.me;

import android.content.Context;

import com.meet.me.database.DatabaseManager;
import com.meet.me.database.tables.Session;

import com.meet.me.database.tables.User;


import org.holoeverywhere.app.Application;

public class MeetMeApplication extends Application {
    public static final String PACKAGE = "com.meet.me";
    private Session mSession;

    private FacebookServiceHelper mServiceHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseManager.getInstance().init(getApplicationContext());
        mServiceHelper = new FacebookServiceHelper(this);
    }

    public void setActiveSession(Session session) {
        mSession = session;
    }

    public Session getActiveSession() {
        return DatabaseManager.getInstance().getHelper().getSessionDao().queryForAll().get(0);
    }

    public User getActiveUser() {
        return getActiveSession().getUser();
    }

    public FacebookServiceHelper getServiceHelper() {
        return mServiceHelper;
    }

    public static MeetMeApplication getApplication(Context context) {
        if (context instanceof MeetMeApplication) {
            return (MeetMeApplication) context;
        }
        return (MeetMeApplication) context;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        DatabaseManager.getInstance().release();
    }
}
