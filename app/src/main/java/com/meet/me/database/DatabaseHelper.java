package com.meet.me.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.meet.me.database.tables.Location;
import com.meet.me.database.tables.Session;
import com.meet.me.database.tables.User;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "meetMe.db";

    private static final int DATABASE_VERSION = 1;

    private RuntimeExceptionDao<User, Integer> userDao = null;
    private RuntimeExceptionDao<Location, Integer> locationDao = null;
    private RuntimeExceptionDao<Session, Integer> sessionDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final Set<Class<?>> mEntities;
    static {
        mEntities = new LinkedHashSet<Class<?>>();
        mEntities.add(Session.class);
        mEntities.add(User.class);
        mEntities.add(Location.class);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        Log.d("TAG", "create DB:" + DATABASE_NAME + " VERSION:" + DATABASE_VERSION);
        try {
            createTables();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        RuntimeExceptionDao<User, Integer> userDao = getRuntimeExceptionDao(User.class);
        RuntimeExceptionDao<Location, Integer> locationDao = getRuntimeExceptionDao(Location.class);
        RuntimeExceptionDao<Session, Integer> sessionDao = getRuntimeExceptionDao(Session.class);

        Location loc1 = new Location(0, 100F, 50F, new Date());

        Location loc2 = new Location(1, 150F, 150F, new Date());
        locationDao.create(loc2);

        //User user1 = new User(100, "FAKENAME", "FAKENAME", "FAKELASTNAME", false);
        //userDao.create(user1);

        //loc1.setUser(user1);
       // locationDao.create(loc1);

        //User user2 = new User(101, "FAKENAME1", "FAKENAME1", "FAKELASTNAME1",false);
       // userDao.create(user2);

        /*
        Session session1 = new Session(100, "accessToken1", new Date());
        sessionDao.create(session1);*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {

    }

    @Override
    public void close() {
        super.close();
        userDao = null;
    }

    private void createTables() throws SQLException {
        for (Class<?> clazz : mEntities) {
            TableUtils.createTable(getConnectionSource(), clazz);
        }
    }

    public RuntimeExceptionDao<User, Integer> getUserDao() {
        if (userDao == null) {
            userDao = getRuntimeExceptionDao(User.class);
        }
        return userDao;
    }

    public RuntimeExceptionDao<Location, Integer> getLocationDao() {
        if (locationDao == null) {
            locationDao = getRuntimeExceptionDao(Location.class);
        }
        return locationDao;
    }

    public RuntimeExceptionDao<Session, Integer> getSessionDao() {
        if (sessionDao == null) {
            sessionDao = getRuntimeExceptionDao(Session.class);
        }
        return sessionDao;
    }
}
