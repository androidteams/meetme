package com.meet.me.database.tables;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = Location.TABLE_NAME)
public class Location {
    public static final String TABLE_NAME = "Location";

    public static final String COL_ID = "_id";
    public static final String COL_LATITUDE = "latitude";
    public static final String COL_LONGITUDE = "longitude";
    public static final String COL_DATE = "date";
    public static final String COL_USER = "user";

    @DatabaseField(id = true, columnName = COL_ID)
    private Integer mId;

    @DatabaseField(columnName = COL_LATITUDE)
    private Float mLatitude;

    @DatabaseField(columnName = COL_LONGITUDE)
    private Float mLongitude;

    @DatabaseField(columnName = COL_DATE, dataType = DataType.DATE)
    private Date  mDate;

    @DatabaseField(foreign = true, columnName = COL_USER)
    private User user;

    public Location() {

    }

    public Location(Integer id, Float latitude, Float longitude, Date date) {
        this.mId = id;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
        this.mDate = date;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
