package com.meet.me.database.tables;

import android.text.TextUtils;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = Session.TABLE_NAME)
public class Session {
    public static final String TABLE_NAME = "Session";

    public static final String COL_CLIENT_ID = "client_id";
    public static final String COL_ACCESS_TOKEN = "access_token";
    public static final String COL_EXPIRES = "expires";

    public Session() {
        // ORMLite needs a no-arg constructor
    }

    @DatabaseField(id = true, columnName = COL_CLIENT_ID)
    private Integer mClientId;

    @DatabaseField(columnName = COL_ACCESS_TOKEN)
    private String mAccessToken;

    @DatabaseField(columnName = COL_EXPIRES, dataType = DataType.DATE)
    private Date mExpires;

    @DatabaseField(foreign = true)
    private User user;

    public Session(Integer clientId, String accessToken, Date expires, User user) {
        this.mClientId = clientId;
        this.mAccessToken = accessToken;
        this.mExpires = expires;
        this.user = user;
    }

    public boolean isActiveSession() {
        return mClientId != 0 && TextUtils.isEmpty(mAccessToken);
    }

    public Integer getClientId() {
        return mClientId;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public Date getExpires() {
        return mExpires;
    }

    public User getUser() {
        return user;
    }
}