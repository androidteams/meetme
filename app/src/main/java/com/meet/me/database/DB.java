package com.meet.me.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DB {
    public static final String COL_ID = "_id";
    public static final String COL_NAME = "name";
    public static final String COL_COURSE = "course";
    public static final String TABLE_STUDENT = "student";
    public static final String COL_PASSWORD = "password";
    public static final String DB_NAME = "university";
    public static final int DB_VERSION = 1;

    private static StringBuffer createTableStudent;

    private DBHelper mDbHelper;
    private SQLiteDatabase mDB;

    private Context mContext;

    static {
        createTableStudent = new StringBuffer("create table ");
        createTableStudent.append(TABLE_STUDENT).append("(");
        createTableStudent.append(COL_ID).append(" integer primary key autoincrement,");
        createTableStudent.append(COL_NAME).append(" text,");
        createTableStudent.append(COL_PASSWORD).append(" text, ");
        createTableStudent.append(COL_COURSE).append(" integer ");
        createTableStudent.append(");");
    }

    public DB(Context context) {
        mContext = context;
    }

    public void open() {
        mDbHelper = new DBHelper(mContext, DB_NAME, null, DB_VERSION);
        mDB = mDbHelper.getWritableDatabase();
    }

    public void close() {
        if (mDbHelper != null) mDbHelper.close();
    }

    public Cursor getAllStudents() {
        return mDB.query(TABLE_STUDENT, null, null, null, null, null, null, null);
    }

    public void addStudent(String name, String password, int course) {
        ContentValues student = new ContentValues();
        student.put(COL_NAME, name);
        student.put(COL_PASSWORD, password);
        student.put(COL_COURSE, course);
        mDB.insert(TABLE_STUDENT, null, student);
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(createTableStudent.toString());

            ContentValues cv = new ContentValues();
            for (int i = 1; i < 5; i++) {
                cv.put(COL_NAME, "Name" + i);
                cv.put(COL_PASSWORD, "Password:" + i);
                cv.put(COL_COURSE, i);
                db.insert(TABLE_STUDENT, null, cv);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
