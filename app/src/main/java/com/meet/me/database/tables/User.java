package com.meet.me.database.tables;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.Date;

@DatabaseTable(tableName = User.TABLE_NAME)
public class User {
    public static final String TABLE_NAME = "User";

    public static final String COL_CLIENT_ID = "client_id";
    public static final String COL_NAME = "name";
    public static final String COL_FIRST_NAME = "first_name";
    public static final String COL_LAST_NAME = "last_name";
    public static final String COL_BIRTHDAY = "birthday";
    public static final String COL_PHONE = "phone";
    public static final String COL_GENDER = "gender";
    public static final String COL_LOCATION = "locations";
    public static final String COL_PHOTO_LINK = "photo_link";
    public static final String COL_PHOTO = "photo";

    @DatabaseField(id = true, columnName = COL_CLIENT_ID)
    private Integer mClientId;

    @DatabaseField(columnName = COL_NAME)
    private String mName;

    @DatabaseField(columnName = COL_FIRST_NAME)
    private String mFirstName;

    @DatabaseField(columnName = COL_LAST_NAME)
    private String mLastName;

    @DatabaseField(columnName = COL_PHONE)
    private String mPhone;

    @DatabaseField(columnName = COL_PHOTO)
    private String mPhoto;

    @DatabaseField(columnName = COL_GENDER)
    private Boolean mGender;

    @DatabaseField(columnName = COL_PHOTO_LINK)
    private String mPhotoLink;

    @ForeignCollectionField(columnName = COL_LOCATION, eager = true)
    private Collection<Location> mLocation;

    User() {
        // ORMLite needs a no-arg constructor
    }

    public User(Integer clientId, String name, String firstName, String lastName, Boolean gender, String photo) {
        this.mName = name;
        this.mFirstName = firstName;
        this.mLastName = lastName;
        this.mClientId = clientId;
        this.mGender = gender;
        this.mPhoto = photo;
    }

    public User(Integer clientId, String name, String firstName, String lastName, String photoLink, Boolean gender) {
        this.mName = name;
        this.mFirstName = firstName;
        this.mLastName = lastName;
        this.mClientId = clientId;
        this.mPhotoLink = photoLink;
        this.mGender = gender;
    }

    public Integer getClientId() {
        return mClientId;
    }

    public String getName() {
        return mName;
    }

    public Collection<Location> getLocation() {
        return mLocation;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setLocation(Collection<Location> location) {
        this.mLocation = location;
    }
}
