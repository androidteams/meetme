package com.meet.me;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.holoeverywhere.widget.Button;

public class SampleFragment extends Fragment {

    private View.OnClickListener mClickStart = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((MeetMeApplication) getActivity().getApplicationContext()).getServiceHelper().performAuthorization("dsadwed34re43rfdwsead");
        }
    };

    private View.OnClickListener mClickStop = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sample, null);

        Button butStart = (Button) rootView.findViewById(R.id.start);
        Button butStop = (Button) rootView.findViewById(R.id.stop);

        butStart.setOnClickListener(mClickStart);
        butStop.setOnClickListener(mClickStop);
        return rootView;
    }
}
