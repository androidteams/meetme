package com.meet.me;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.SparseArray;

import com.meet.me.command.Command;
import com.meet.me.command.api.Error;
import com.meet.me.command.api.HttpCode;
import com.meet.me.command.api.HttpRequest;
import com.meet.me.command.api.RequestListener;
import com.meet.me.command.api.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ServiceHelper {

    private List<RequestListener> mCurrentListeners;
    private SparseArray<Intent> mPendingActivities = new SparseArray<Intent>();
    private AtomicInteger mGenerateRequestId = new AtomicInteger();

    private Application mApp;

    public ServiceHelper(Application application) {
        mApp = application;
        mCurrentListeners = new ArrayList<RequestListener>();
    }

    public Application getApp() {
        return mApp;
    }

    public Context getContext() {
        return mApp.getApplicationContext();
    }

    public int getGeneratedRequestId() {
        return mGenerateRequestId.getAndIncrement();
    }

    public void addListener(RequestListener listener) {
        mCurrentListeners.add(listener);
    }

    public void removeListener(RequestListener listener) {
        mCurrentListeners.remove(listener);
    }

    public Intent createIntent(final Context context, final Command command, final int requestId) {
        Intent intent = new Intent(context, CommandService.class);
        intent.setAction(CommandService.ACTION_EXECUTE_COMMAND);

        intent.putExtra(CommandService.EXTRA_COMMAND, command);
        intent.putExtra(CommandService.EXTRA_REQUEST_ID, requestId);
        intent.putExtra(CommandService.EXTRA_STATUS_RECEIVER, new MyResultReceiver(requestId));
        return intent;
    }

    public boolean check(Intent intent, Class<? extends Command> clazz) {
        Parcelable commandExtra = intent.getParcelableExtra(CommandService.EXTRA_COMMAND);
        return commandExtra != null && commandExtra.getClass().equals(clazz);
    }

    public int runRequest(final int requestId, Intent intent) {
        mPendingActivities.append(requestId, intent);
        mApp.startService(intent);
        return requestId;
    }

    public boolean isPending(int requestId) {
        return mPendingActivities.get(requestId) != null;
    }

    private class MyResultReceiver extends ResultReceiver {

        private int mRequestId;
        public MyResultReceiver(int requestId) {
            super(new Handler());
            mRequestId = requestId;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Intent originalIntent = mPendingActivities.get(mRequestId);
            if (isPending(mRequestId)) {
                if (resultCode != Command.RESPONSE_PROGRESS) {
                    mPendingActivities.remove(mRequestId);
                }
            }

            for (RequestListener listener : mCurrentListeners) {
                if (listener != null) {
                    sendResponse(resultCode, originalIntent, resultData, listener);
                }
            }

        }

        private void sendResponse(int resultCode, Intent requestIntent, Bundle resultData, RequestListener listener) {
            HttpCode code = HttpCode.getHttpCode(resultCode);

            switch (resultCode) {
                case Command.RESPONSE_SUCCESS:
                    listener.onCompleted(mRequestId, requestIntent, new Response(code, getResponse(resultData), resultData));
                    break;
                case Command.RESPONSE_FAILURE:
                    listener.onError(mRequestId, requestIntent, new Error(code, getResponse(resultData)));
                    break;
                case Command.RESPONSE_PROGRESS:
                    break;
            }
        }

        private String getResponse(Bundle result) {
            return result.getString(HttpRequest.EXTRA_RESPONSE);
        }

    }
}
