package com.meet.me.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.meet.me.MeetMeApplication;
import com.meet.me.R;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by SCV on 17.04.2014.
 */
public class ImageFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.image, null);

        Log.d("ImageDownload", "ImageFragment start loading");
        ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
       // ((MeetMeApplication) getActivity().getApplicationContext()).getServiceHelper().loadImage(imageView, "http://toys.ua/products_pictures/a_11532_l.jpg");
        Picasso picasso = new Picasso.Builder(getActivity().getApplicationContext())
                .downloader(new OkHttpDownloader(getActivity().getApplicationContext())).build();

        picasso.setDebugging(true);
        picasso.load("http://toys.ua/products_pictures/a_11532_l.jpg")
                .resize(100, 100)
                .centerCrop()
                .into(imageView);

        return rootView;
    }


}
