package com.meet.me.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.meet.me.FacebookServiceHelper;
import com.meet.me.MeetMeApplication;
import com.meet.me.R;
import com.meet.me.adapter.FeedAdapter;
import com.meet.me.command.api.*;
import com.meet.me.command.facebook.Edge;
import com.meet.me.command.facebook.Node;

public class FeedFragment extends Fragment {

    private ProgressDialogFragment mProgressDialog;

    private FacebookServiceHelper mServiceHelper;
    private FeedAdapter mFeedAdapter;

    RequestListener mGetFeeds = new RequestListener() {

        @Override
        public void onCompleted(int requestId, Intent requestIntent, Response response) {
            if (mServiceHelper.checkFacebook(requestIntent, Node.ME, Edge.HOME)) {
                mProgressDialog.dismiss();
            }
        }

        @Override
        public void onError(int requestId, Intent requestIntent, com.meet.me.command.api.Error error) {

        }

        @Override
        public void onProgress(int requestId, Intent requestIntent, HttpRequest.ProgressType progressType, long bytesLoaded, long bytesTotal) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        ((MeetMeApplication) getActivity().getApplication()).getServiceHelper().addListener(mGetFeeds);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((MeetMeApplication) getActivity().getApplication()).getServiceHelper().removeListener(mGetFeeds);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceHelper = ((MeetMeApplication) getActivity().getApplication()).getServiceHelper();

        mProgressDialog = new ProgressDialogFragment();
        mProgressDialog.show(getActivity().getSupportFragmentManager(), null);

        mFeedAdapter = new FeedAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.feed_list, null);

        ListView feedList = (ListView) rootView.findViewById(R.id.feed_list);
        feedList.setAdapter(mFeedAdapter);

        return rootView;
    }
}
