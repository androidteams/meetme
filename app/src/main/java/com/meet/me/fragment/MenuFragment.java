package com.meet.me.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.meet.me.MeetMeApplication;
import com.meet.me.R;
import com.meet.me.SlidingMenu;
import com.meet.me.adapter.MenuAdapter;
import com.meet.me.adapter.MenuItemAdapter;
import com.meet.me.adapter.SectionAdapter;
import com.meet.me.database.DatabaseManager;
import com.meet.me.database.tables.Session;
import com.meet.me.database.tables.User;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * ---Profile---
 * Friends
 * Feed
 * Messages
 * Photos
 *
 * -----Meets--------
 * Meets
 * Invites
 * Nearby (Bluetooth)
 *
 * ----Dating--
 * Find a mate
 * They like me
 * Favorites
 *
 * ---- Tools---
 * Settings
 * Send feedback
 * Rate this app
 * Themes
 *
 * */
public class MenuFragment extends Fragment {
    private ListAdapter mAdapter;
    private View mCurSelectedItem;
    
    AuthorizationFragment fragment = new AuthorizationFragment();

    private AdapterView.OnItemClickListener mMenuItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Object menuObject = parent.getItemAtPosition(position);

            if (menuObject instanceof MenuItemAdapter.MenuItem) {
                MenuItemAdapter.MenuItem menuItem = (MenuItemAdapter.MenuItem) menuObject;
                ((SlidingMenu) getActivity()).switchFragment(menuItem.getFragment());
                highlightSelectedItem(view);
            }
        }
    };

    private View.OnClickListener mTitleBarClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            User user = ((MeetMeApplication) getActivity().getApplication()).getActiveUser();
            ((SlidingMenu) getActivity()).switchFragment(new ProfileFragment(user));
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new MenuAdapter(getActivity(), getSections());
    }

     private SectionAdapter.Section[] getSections() {
        SectionAdapter.Section profile = new SectionAdapter.Section(getProfileSection());
        SectionAdapter.Section meets = new SectionAdapter.Section(getString(R.string.section_meets), getMeetSection());
        SectionAdapter.Section like = new SectionAdapter.Section(getString(R.string.section_dating), getDatingSection());
        SectionAdapter.Section tools = new SectionAdapter.Section(getString(R.string.section_tools), getToolSection());

        return new SectionAdapter.Section[] { profile, meets, like, tools };
    }

    private Adapter getProfileSection() {
        List<MenuItemAdapter.MenuItem> menuItems = new ArrayList<MenuItemAdapter.MenuItem>();

        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.facebook_item_friends), R.drawable.ic_launcher, new FriendFragment()));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.facebook_item_feed), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.facebook_item_messages), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.facebook_item_photo), R.drawable.ic_launcher, new ImageFragment()));
        return new MenuItemAdapter(getActivity(), menuItems);
    }

    private Adapter getMeetSection() {
        List<MenuItemAdapter.MenuItem> menuItems = new ArrayList<MenuItemAdapter.MenuItem>();
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.meets_item_meets), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.meets_item_invites), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.meets_item_nearby), R.drawable.ic_launcher, fragment));
        return new MenuItemAdapter(getActivity(), menuItems);
    }

    private Adapter getDatingSection() {
        List<MenuItemAdapter.MenuItem> menuItems = new ArrayList<MenuItemAdapter.MenuItem>();
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.dating_item_find_a_mate), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.dating_item_they_like_me), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.dating_item_favorites), R.drawable.ic_launcher, fragment));
        return new MenuItemAdapter(getActivity(), menuItems);
    }

    private Adapter getToolSection() {
        List<MenuItemAdapter.MenuItem> menuItems = new ArrayList<MenuItemAdapter.MenuItem>();
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.tools_item_settings), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.tools_item_send_feedback), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.tools_item_rate_this_app), R.drawable.ic_launcher, fragment));
        menuItems.add(new MenuItemAdapter.MenuItem(getString(R.string.tools_item_themes), R.drawable.ic_launcher, fragment));
        return new MenuItemAdapter(getActivity(), menuItems);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.menu, null);


        RelativeLayout titleBar = (RelativeLayout) rootView.findViewById(R.id.name_bar);
        titleBar.setOnClickListener(mTitleBarClick);

        ImageView avatar = (ImageView) rootView.findViewById(R.id.avatar);
        TextView userName = (TextView) rootView.findViewById(R.id.name);

        ListView listView = (ListView) rootView.findViewById(R.id.menu);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(mMenuItemClick);
        return rootView;
    }

    private void highlightSelectedItem(View view) {
        if (mCurSelectedItem != null) {
            mCurSelectedItem.setBackgroundDrawable(null);
        }

        view.setBackgroundDrawable(getResources().getDrawable(R.color.menu_item_activated));
        mCurSelectedItem = view;
    }
}
