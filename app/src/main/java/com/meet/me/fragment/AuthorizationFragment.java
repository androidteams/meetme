package com.meet.me.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.meet.me.R;
import com.meet.me.SplashScreenActivity;

public class AuthorizationFragment extends Fragment {

    private View.OnClickListener mSignUpClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((SplashScreenActivity) getActivity()).switchToRegistration();
        }
    };

    private View.OnClickListener mSignInClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((SplashScreenActivity) getActivity()).switchToLogin();
        }
    };

    private View.OnClickListener mSignInVkClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((SplashScreenActivity) getActivity()).switchToFacebookAuthrization();
        }
    };

    public AuthorizationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.authorization, container, false);
        Button signInVk = (Button) rootView.findViewById(R.id.connect_with_facebook);
        Button signIn = (Button) rootView.findViewById(R.id.sign_in);
        Button signUp = (Button) rootView.findViewById(R.id.sign_up);

        signInVk.setOnClickListener(mSignInVkClick);
        signUp.setOnClickListener(mSignUpClick);
        signIn.setOnClickListener(mSignInClick);
        return rootView;
    }
}

