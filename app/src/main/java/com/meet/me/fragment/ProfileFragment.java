
package com.meet.me.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.meet.me.FacebookServiceHelper;
import com.meet.me.MeetMeApplication;
import com.meet.me.R;
import com.meet.me.command.api.*;
import com.meet.me.database.tables.User;

public class ProfileFragment extends Fragment {

    private FacebookServiceHelper mServiceHelper;
    private User mUser;

    private TextView userName;
    private ProgressDialogFragment progressDialog;

    private RequestListener mGetUserInfo = new RequestListener() {
        @Override
        public void onCompleted(int requestId, Intent requestIntent, Response response) {

            progressDialog.dismiss();
        }

        @Override
        public void onError(int requestId, Intent requestIntent, com.meet.me.command.api.Error error) {

        }

        @Override
        public void onProgress(int requestId, Intent requestIntent, HttpRequest.ProgressType progressType, long bytesLoaded, long bytesTotal) {

        }
    };

    public ProfileFragment(User user) {
        this.mUser = user;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceHelper = ((MeetMeApplication) getActivity().getApplication()).getServiceHelper();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.profile, null);

        ImageView avatar = (ImageView) rootView.findViewById(R.id.avatar_img);
        userName = (TextView) rootView.findViewById(R.id.user_name);
        TextView userWork = (TextView) rootView.findViewById(R.id.work);
        TextView userStudy = (TextView) rootView.findViewById(R.id.studied);
        TextView userLive = (TextView) rootView.findViewById(R.id.lives);

        if (TextUtils.isEmpty(mUser.getName())) {
            loadUser();
        } else {
            userName.setText(mUser.getName());
        }

        return rootView;
    }

    private void showDialog() {
        progressDialog = new ProgressDialogFragment();
        progressDialog.show(getActivity().getSupportFragmentManager(), null);
    }

    private void loadUser() {
        showDialog();

        String accessToken = ((MeetMeApplication) getActivity().getApplication()).getActiveSession().getAccessToken();
        mServiceHelper.getUserInfo(accessToken, mUser.getClientId().toString());
    }
}

