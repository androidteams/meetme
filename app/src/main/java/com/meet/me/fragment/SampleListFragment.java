package com.meet.me.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.CursorLoader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;

import com.meet.me.R;
import com.meet.me.database.DB;

import java.util.concurrent.TimeUnit;

public class SampleListFragment extends ListFragment {

    private DB mDb;

    private SimpleCursorAdapter mAdapter;

    private LoaderManager.LoaderCallbacks<Cursor> mCursorLoaderCallbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args)
        {
            return new MyCursorLoader(getActivity(), mDb);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mAdapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDb = new DB(getActivity());
        mDb.open();

        String[] from = new String[] { DB.COL_NAME, DB.COL_PASSWORD, DB.COL_COURSE };
        int[] to = new int[] { R.id.name, R.id.password, R.id.course };

        mAdapter = new SimpleCursorAdapter(getActivity(), R.layout.row, null, from , to, 0);
        setListAdapter(mAdapter);

        getActivity().getSupportLoaderManager().initLoader(0, null, mCursorLoaderCallbacks).forceLoad();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.friend_list, null);

        Button addStudent = (Button) rootView.findViewById(R.id.addstudent);

        addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDb.addStudent("Name" + 100, "student123", 100);
                getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
            }
        });

        return rootView;
	}


    static class MyCursorLoader extends CursorLoader {

        private DB mDB;

        public MyCursorLoader(Context context, DB db) {
            super(context);
            mDB = db;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = mDB.getAllStudents();
            return cursor;
        }
    }
}
