package com.meet.me.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.meet.me.FacebookServiceHelper;
import com.meet.me.MeetMeApplication;
import com.meet.me.R;
import com.meet.me.SlidingMenu;
import com.meet.me.adapter.FriendsAdapter;
import com.meet.me.command.api.HttpRequest;
import com.meet.me.command.api.RequestListener;
import com.meet.me.command.api.Response;
import com.meet.me.command.facebook.Edge;
import com.meet.me.command.facebook.Node;
import com.meet.me.database.DatabaseManager;
import com.meet.me.database.tables.Session;
import com.meet.me.database.tables.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FriendFragment extends Fragment {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String PICTURE = "picture";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String URL = "url";
    public static final String GENDER = "gender";
    public static final String MALE = "male";

    private FriendsAdapter mAdapter;

    private FacebookServiceHelper mServiceHelper;

    private ProgressDialogFragment progressDialog;

    RequestListener mGetFriends = new RequestListener() {
        @Override
        public void onCompleted(int requestId, Intent requestIntent, Response response) {
            if (mServiceHelper.checkFacebook(requestIntent, Node.ME, Edge.FRIENDS)) {
                try {
                    List<User> friends = new ArrayList<User>();
                    RuntimeExceptionDao<User, Integer> userDAO =  DatabaseManager.getInstance().getHelper().getUserDao();

                    JSONObject data = new JSONObject(response.getResponseData());
                    JSONArray arr = data.getJSONArray("data");

                    User user;
                    Integer clientId;
                    JSONObject picData;

                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject frd = arr.getJSONObject(i);

                        clientId = frd.getInt(ID);
                        picData = frd.getJSONObject(PICTURE).getJSONObject("data");

                        user = new User(clientId, frd.getString(NAME), frd.getString(FIRST_NAME),
                                frd.getString(LAST_NAME), picData.getString(URL),
                                    frd.getString(GENDER).equals(MALE) ? true : false);

                        friends.add(user);
                        if (userDAO.idExists(clientId)) {
                            userDAO.update(user);
                        } else {
                            userDAO.create(user);
                        }
                    }
                    updateFriends(friends);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
           progressDialog.dismiss();
        }

        @Override
        public void onError(int requestId, Intent requestIntent, com.meet.me.command.api.Error error) {

        }

        @Override
        public void onProgress(int requestId, Intent requestIntent, HttpRequest.ProgressType progressType, long bytesLoaded, long bytesTotal) {

        }
    };

    private AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            User user = (User) parent.getItemAtPosition(position);
            ((SlidingMenu) getActivity()).switchFragment(new ProfileFragment(user));
            Log.d("TAG", "");
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        ((MeetMeApplication) getActivity().getApplication()).getServiceHelper().addListener(mGetFriends);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((MeetMeApplication) getActivity().getApplication()).getServiceHelper().removeListener(mGetFriends);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceHelper = ((MeetMeApplication) getActivity().getApplication()).getServiceHelper();

        progressDialog = new ProgressDialogFragment();
        progressDialog.show(getActivity().getSupportFragmentManager(), null);

        mAdapter = new FriendsAdapter(getActivity(), getFriends());
        List<Session> sessions = DatabaseManager.getInstance().getHelper().getSessionDao().queryForAll();
        if (sessions.size() > 0) {
            mServiceHelper.getFriends(sessions.get(0).getAccessToken());
        }
    }

    private void updateFriends(List<User> friends) {
        mAdapter.setFriends(friends);
        mAdapter.notifyDataSetChanged();
    }

    private List<User> getFriends() {
        List<User> frds = new ArrayList<User>();
        return frds;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.friends_list, null);

        ListView listView = (ListView) rootView.findViewById(R.id.frd_list);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(onItemClick);
        return rootView;
    }
}
