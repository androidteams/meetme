package com.meet.me.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.twotoasters.clusterkraf.Clusterkraf;
import com.twotoasters.clusterkraf.InputPoint;
import com.twotoasters.clusterkraf.Options;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MeetMapFragment extends SupportMapFragment {

    private Map<Marker, Object> markerContainer;
    private GoogleMap mMap;
    private Clusterkraf mClusterkraf;
    private CameraPosition resoreCamera;
    ArrayList<InputPoint> mInputPoints;

    public MeetMapFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mapView = super.onCreateView(inflater, container, savedInstanceState);
        markerContainer = new HashMap<Marker, Object>();
        initMap();
        initClusterkraf();
        return mapView;
    }

    private void initMap() {
        mMap = getMap();
        LatLng sydney = new LatLng(-33.867, 151.206);

        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));

        final Marker marker1 = mMap.addMarker(new MarkerOptions()
                .title("Sydney")
                .snippet("The most populous city in Australia.")
                .position(sydney).icon(BitmapDescriptorFactory.fromResource(com.meet.me.R.drawable.ic_launcher)));
        markerContainer.put(marker1, "Hello");

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (markerContainer.containsKey(marker)) {
                    String data = markerContainer.get(marker1).toString();
                    Toast.makeText(getActivity(), data, 500).show();
                }
                return true;
            }
        });
    }

    private void initClusterkraf() {
        if (mMap != null && mInputPoints != null && mInputPoints.size() > 0) {
            Options options = new Options();
            mClusterkraf = new Clusterkraf(mMap, options, mInputPoints);
        }
    }
}
