package com.meet.me.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.meet.me.database.DatabaseManager;
import com.meet.me.database.tables.User;
import com.meet.me.R;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;

import java.util.List;

public class DBFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dbview, container, false);
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RuntimeExceptionDao<User, Integer> userDao = DatabaseManager.getInstance().getHelper().getUserDao();
        List<User> users = userDao.queryForAll();
    }
}
