package com.meet.me.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.meet.me.CheckInputData;
import com.meet.me.R;
import com.meet.me.widget.CheckEditText;
import com.meet.me.widget.DatePicker;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.datetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class RegistrationFragment extends Fragment {

    private CheckEditText mFirstName;
    private CheckEditText mLastName;
    private CheckEditText mEmail;
    private CheckEditText mPassword;
    private CheckEditText mPhone;
    private DatePicker mDatePicker;
    private Button mDone;

    private GregorianCalendar mCurDate;
    private GregorianCalendar mBirthDayDate;

    private View.OnClickListener mDatePickerClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showDateDialog();
        }
    };

    private DatePickerDialog.OnDateSetListener mDialogListener  = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year,
                              int monthOfYear, int dayOfMonth) {
            mCurDate = new GregorianCalendar(year, monthOfYear, dayOfMonth);
            mDatePicker.setBirthday(mCurDate);
        }
    };

    private View.OnClickListener mDoneClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            performRegistration();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Context regTheme = new ContextThemeWrapper(getActivity(), R.style.Registraion);
        inflater = LayoutInflater.from(regTheme);
        View rootView = inflater.inflate(R.layout.registration, container, false);
        mFirstName = (CheckEditText) rootView.findViewById(R.id.first_name);
        mLastName = (CheckEditText) rootView.findViewById(R.id.last_name);
        mEmail = (CheckEditText) rootView.findViewById(R.id.email);
        mPassword = (CheckEditText) rootView.findViewById(R.id.password);
        mPhone = (CheckEditText) rootView.findViewById(R.id.phone);
        mDatePicker = (DatePicker) rootView.findViewById(R.id.birthday);
        mDone = (Button) rootView.findViewById(R.id.done);

        initializeBirthday();

        setListeners();
        setCheckers();
        return rootView;
    }

    private void initializeBirthday() {
        mBirthDayDate = new GregorianCalendar();
        mBirthDayDate.roll(Calendar.YEAR, -1);
        mCurDate = mBirthDayDate;
    }

    public void setListeners() {
        mDatePicker.setOnClickListener(mDatePickerClick);
        mDone.setOnClickListener(mDoneClick);
    }

    public void setCheckers() {
        mFirstName.setOnCheckInputData(new CheckFirstName());
    }

    public void showDateDialog() {
        DatePickerDialog dialog = DatePickerDialog.newInstance(mDialogListener, mCurDate.get(GregorianCalendar.YEAR), mCurDate.get(GregorianCalendar.MONTH), mCurDate.get(GregorianCalendar.DAY_OF_MONTH));
        dialog.setYearRange(1900, mBirthDayDate.get(GregorianCalendar.YEAR));
        dialog.show(getSupportActivity());
    }

    public void performRegistration() {
        if (isRegistrationDataRight()) {

        }
    }

    public boolean isRegistrationDataRight() {
        return mFirstName.isCheck();
    }

    private class CheckFirstName implements CheckInputData {

        @Override
        public boolean isInputDataRight(String textField) {
            return true;
        }
    }
}
