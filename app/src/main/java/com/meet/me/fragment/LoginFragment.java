package com.meet.me.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.meet.me.R;
import com.meet.me.SlidingMenu;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;


public class LoginFragment extends Fragment {
    private EditText mUserName;
    private EditText mPassword;

    private View.OnClickListener mClickSignIn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), SlidingMenu.class);
            startActivity(intent);
            getActivity().finish();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login, null);
        mUserName = (EditText) view.findViewById(R.id.username);
        mPassword = (EditText) view.findViewById(R.id.password);
        Button signIn = (Button) view.findViewById(R.id.sign_in);

        signIn.setOnClickListener(mClickSignIn);
        return view;
    }
}
