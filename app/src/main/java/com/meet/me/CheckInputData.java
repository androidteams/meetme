package com.meet.me;

public interface CheckInputData {
    boolean isInputDataRight(String textField);
}
