package com.meet.me.util;

public enum Fonts {
    ROBOTO_BOLD_TTF("Roboto-Bold"), ROBOTO_LIGHT_TTF("Roboto-Bold"),  ROBOTO_REGULAR_TTF("Roboto-Bold"), ROBOTO_ITALIC_TTF("Roboto-Italic"), LOVE_AND_PASSION_TTF("Love_and_Passion");

    private String mFont;
    private Fonts(String font) {
        this.mFont = font;
    }

    public String getFontName() {
        return mFont;
    }
}
