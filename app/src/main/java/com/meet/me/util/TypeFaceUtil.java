package com.meet.me.util;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

public class TypeFaceUtil {
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    private static Typeface get(Context context, String assetPath) {
        synchronized (cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    Typeface typeface = Typeface.createFromAsset(context.getAssets(), assetPath);
                    cache.put(assetPath, typeface);
                } catch (Exception e) {
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }

    public static Typeface getTypeface(Context context, String typeFaceName) {
        return get(context, typeFaceName);
    }
}
