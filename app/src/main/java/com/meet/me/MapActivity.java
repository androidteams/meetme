package com.meet.me;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.meet.me.fragment.MeetMapFragment;

import org.holoeverywhere.app.Activity;

public class MapActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_content_frame);
        switchFragment(new MeetMapFragment());
    }

    public void switchFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.map_content_frame, fragment)
                    .commit();
        }
    }
}
