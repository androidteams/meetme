package com.meet.me;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;

import com.meet.me.fragment.SampleListFragment;

public class SlidingMenu extends SlidingBaseActivity {
    private Button mMap;
    private Button mMenu;

    private View.OnClickListener mClickToMap = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(SlidingMenu.this, MapActivity.class);
            startActivity(intent);
        }
    };

    private View.OnClickListener mClickToMenu = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!getSlidingMenu().isMenuShowing()) {
                getSlidingMenu().showMenu();
            }
        }
    };

    public SlidingMenu() {
        super(R.string.app_name);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_frame);

        mMap = (Button) findViewById(R.id.map);
        mMenu = (Button) findViewById(R.id.menu);

        mMap.setOnClickListener(mClickToMap);
        mMenu.setOnClickListener(mClickToMenu);

        switchFragment(new SampleListFragment());
    }

    public void switchFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();
        }
    }
}
