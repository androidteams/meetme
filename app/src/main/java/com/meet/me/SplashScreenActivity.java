package com.meet.me;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.meet.me.database.DatabaseManager;
import com.meet.me.database.tables.Session;
import com.meet.me.fragment.AuthorizationFragment;
import com.meet.me.fragment.LoginFragment;
import com.meet.me.fragment.RegistrationFragment;
import com.meet.me.fragment.SampleListFragment;

import java.util.List;

public class SplashScreenActivity extends BaseActivity {

    public static final int AUTHORIZE_REQUEST_CODE = 100;
    private RegistrationFragment mRegistration;
    private AuthorizationFragment mAuthorization;
    private LoginFragment mLogin;
    private FacebookAuthorizationActivity mFacebookAuthorization;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!isClientIdExist()) {
            switchToAuthorization();
        } else {
            goToSlidingMenu();
        }
    }

    private void goToSlidingMenu() {
        Intent intent = new Intent(this, SlidingMenu.class);
        startActivity(intent);
        finish();
    }

    public boolean isClientIdExist() {
        List<Session> sessions = DatabaseManager.getInstance().getHelper().getSessionDao().queryForAll();
        if (sessions.size() > 0) {
            return true;
        }
        return false;
    }

    private int getFragmentId() {
        return R.id.container;
    }

    public void switchToAuthorization() {
        if (mAuthorization == null) {
            mAuthorization = new AuthorizationFragment();
            addFragment(mAuthorization);
        } else {
            replaceFragment(mAuthorization);
        }
    }

    public void switchToFacebookAuthrization() {
        Intent intent = new Intent(this, FacebookAuthorizationActivity.class);
        startActivityForResult(intent, AUTHORIZE_REQUEST_CODE);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTHORIZE_REQUEST_CODE) {
            goToSlidingMenu();
        }
    }

    public void switchToLogin() {
        if (mLogin == null) {
            mLogin = new LoginFragment();
        }
        replaceFragment(mLogin);
    }

    public void switchToRegistration() {
        if (mRegistration == null) {
            mRegistration = new RegistrationFragment();
        }
        replaceFragment(mRegistration);
    }

    private void addFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(getFragmentId(), fragment).commit();
    }

    private void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(getFragmentId(), fragment);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, true);
    }
}
