package com.meet.me.tests.ui;

import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.meet.me.MainActivity;
import com.meet.me.R;

public class SimpleMainActivity extends ActivityInstrumentationTestCase2 {

    private Solo mSolo;

    public SimpleMainActivity() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mSolo = new Solo(getInstrumentation(), getActivity());
    }

    public void testMainActivity() throws Exception {
        // check that we have the right activity
        mSolo.assertCurrentActivity("MainActivity", MainActivity.class);
        assertEquals(true, true);

        /*String butTitle = mSolo.getString(R.id.button);
        mSolo.clickOnButton(butTitle);*/
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        //mSolo.finishOpenedActivities();
    }
}
